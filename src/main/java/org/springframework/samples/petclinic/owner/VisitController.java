/*
 * Copyright 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.owner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.vet.VetRepository;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.samples.petclinic.visit.VisitDto;
import org.springframework.samples.petclinic.visit.VisitRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Optional;

/**
 * @author Juergen Hoeller
 * @author Ken Krebs
 * @author Arjen Poutsma
 * @author Michael Isvy
 * @author Dave Syer
 */
@Controller
class VisitController {

    private final VisitRepository visits;
    private final PetRepository pets;
    private final VetRepository vets;
    private Visit visit;
    private BindingResult result;


    public VisitController(VisitRepository visits, PetRepository pets, VetRepository vets) {
        this.visits = visits;
        this.pets = pets;
        this.vets = vets;
    }

    @InitBinder
    public void setAllowedFields(WebDataBinder dataBinder) {
        dataBinder.setDisallowedFields("id");
    }

    /**
     * Called before each and every @RequestMapping annotated method.
     * 2 goals:
     * - Make sure we always have fresh data
     * - Since we do not use the session scope, make sure that Pet object always has an id
     * (Even though id is not part of the form fields)
     *
     * @param petId
     * @return Pet
     */
    @ModelAttribute("visit")
    public Visit loadPetWithVisit(@PathVariable("petId") int petId, ModelMap model) {
        Pet pet = this.pets.findById(petId);
        model.put("pet", pet);
        Visit visit = new Visit();
        pet.addVisit(visit);
        return visit;
    }

    @ModelAttribute("vets")
    public Collection<Vet> loadVets(ModelMap model) {
        Collection<Vet> vets = this.vets.findAll();
        model.put("vets", vets);
        return vets;
    }

    // Spring MVC calls method loadPetWithVisit(...) before initNewVisitForm is called
    @GetMapping("/owners/*/pets/{petId}/visits/new")
    public String initNewVisitForm(@PathVariable("petId") int petId, ModelMap model) {
        Visit visit = new Visit();
        model.put("visit", visit);
        return "pets/createOrUpdateVisitForm";
    }

    @GetMapping("/owners/*/pets/{petId}/visits/{visitId}/update")
    public String initUpdateVisitForm(@PathVariable("petId") int petId, @PathVariable("visitId") int visitId, ModelMap model) {
        Visit visit = this.visits.findById(visitId).get();
        model.put("visit", visit);
        return "pets/createOrUpdateVisitForm";
    }

    @PostMapping("/owners/{ownerId}/pets/{petId}/visits/{visitId}/update")
    public String processUpdateVisitForm(@Valid VisitDto visitDto, @PathVariable Integer visitId,
                                         BindingResult result) {
        if (result.hasErrors()) {
            return "pets/createOrUpdateVisitForm";
        } else {
            Visit visit = new Visit();
            visit.setId(visitId);
            visit.setVet(this.vets.findById(visitDto.getVetId()));
            visit.setPetId(visitDto.getPetId());
            visit.setDescription(visitDto.getDescription());
            visit.setDate(visitDto.getDate());
            this.visits.save(visit);
            return "redirect:/owners/{ownerId}";
        }
    }

    // Spring MVC calls method loadPetWithVisit(...) before processNewVisitForm is called
    @PostMapping("/owners/{ownerId}/pets/{petId}/visits/new")
    public String processNewVisitForm(@Valid VisitDto visitDto, BindingResult result) {

        if (result.hasErrors()) {
            return "pets/createOrUpdateVisitForm";
        } else {
            Visit visit = new Visit();
            visit.setVet(this.vets.findById(visitDto.getVetId()));
            visit.setPetId(visitDto.getPetId());
            visit.setDescription(visitDto.getDescription());
            visit.setDate(visitDto.getDate());
            this.visits.save(visit);
            return "redirect:/owners/{ownerId}";
        }
    }

    @PostMapping("/owners/{ownerId}/pets/{petId}/visits/{visitId}/delete")
    public String deleteVisit(@PathVariable Integer visitId) {
        this.visits.setDeleted(visitId);
        return "redirect:/owners/{ownerId}";
    }

    // get if not deleted

}
